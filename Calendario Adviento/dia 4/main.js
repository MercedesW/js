/*
Given a year, return the century it is in.
The first century spans from the year 1 up to and including the year 100,
the second - from the year 101 up to and including the year 200, etc
*/



/* Código */
function centuryFromYear(num) {
	let years = Math.floor(num/100);
	if(num % 100)
		years += 1;
	return years;
}

/* Test Jasmine */
describe("centuryFromYear()", function() {
	it("returns current century", function() {
		const year = 1905;
		const result = centuryFromYear(year);
		expect(result).toBe(20);
	})

	it("returns current century for edge case of start of century", function() {
		const year = 1700;
		const result = centuryFromYear(year);
		expect(result).toBe(17);
	})
})