/*
DESCRIPTION:
Give an array of integers, find the pair of adjacent elements that has the largest product
and return that product.

*/
function adjacentElementsProduct(nums) {
    //  write code here.
}



/**
* Test Suite 
*/
describe('adjacentElementsProduct()', () => {
    it('returns largest product of adjacent values', () => {
        // arrange
        const nums = [3, 6, -2, -5, 7, 3];
        
        // act
        const result = adjacentElementsProduct(nums);

        // log
        console.log("result: ", result);
        
        // assert
        expect(result).toBe(21);
    });
});