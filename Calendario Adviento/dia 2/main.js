/* You have deposited a specific amount of dollars into your bank account.
Each year your balance increases at the same growth rate.
Find out how long it would take for your balance to pass a specific threshold
with the assumption that you don't make any additional deposits.
*/


/* Código */
function depositProfit(deposit, rate, threshold) {
    let i = deposit;
    let year = 0;
    while (i <= threshold) {
        i += ((rate * i) / 100);
        year += 1;
    }
    return year;
}

/* Test Jasmine */
describe("depositProfit()", function() {
    it("returns number of years it will take to hit threshold based off of deposit & rate", function() {
        const deposit = 100;
        const rate = 20;
        const threshold = 170;

        const result = depositProfit(deposit, rate, threshold);

        expect(result).toBe(3);
    })
})