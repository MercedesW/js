/*
n children have got m pieces of candy.
They want to eat as much candy as they can, but each child must eat
exactly the same amount of candy as any other child.
Determine how many pieces of candy will be eaten by all the children together.
Individual pieces of candy cannot be split.
*/

/* Código */
function candies(children, candy) {
    let pieces = 0;
    if (children > 0 && candy >= 0)
        /* let pieces = candy - (candy % children); */
        pieces = Math.floor(candy/children) * children;
    return pieces;
}

/* Test Jasmine */
describe("candies()", function() {
    it("Returns ammount of total equal candy to be eaten", function() {
        const children = 3;
        const candy = 10;
        const result = candies(children, candy);
        console.log(result)
        expect(result).toBe(9);
    });
    it("without children", function() {
        const children = 0
        const candy = 10;
        const result = candies(children, candy);
        expect(result).toBe(0);
    });
    it("negative candies", function() {
        const children = 3;
        const candy = -10;
        const result = candies(children, candy);
        expect(result).toBe(0);
    });
});
