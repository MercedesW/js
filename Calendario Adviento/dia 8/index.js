/*
DESCRIPTION:
In this challenge a casino has asked you to make an online dice that works just like 
it wold in real life. Using the pre-made dice face that represents ‘one’, make the 
faces for ‘two’, ‘three’, ‘four’, ‘five’ and ‘six’. Now when the users clicks the 
dice on the screen the dice is expected to show one of the faces randomly.

event listeners, Math.random()

*/
/*

DETAILED INSTRUCTIONS
1. pick out the neccesary elements from the HTML
2. Create other 5 dice faces in CSS
3. use eventlisteners on the appropriate div
4. Display dice faces randomly on click

STRETCH GOALS:
- Can you show the number you rolled as a integer along-side the dice face?
- Can you improve the overall design?
*/

function getRandom() {
    let array = ['dot', 'two_dots', 'three_dots', 'four_dots', 'five_dots', 'six_dots'];
    let position = Math.floor(Math.random() * 6);
    return array[position];
}

/* Test Jasmine */
describe("random()", function() {
    it("position", function() {
        let result = getRandom();
        console.log(result);
	})
})
