/*
Reverse the provided string.
You may need to turn the string into an array before you can reverse it.
Your result must be a string.
*/


/* Código */
function reverseAString(str) {
    let array = str.split("");
    let reverseArray = array.reverse();
    let newString = reverseArray.join("");
    return newString;
}

/* Test Jasmine */
describe("reverseAString()", function() {
    it("returns original string reversed", function() {
        const str = 'hello';
        const result = reverseAString(str);
        expect(result).toBe('olleh');
    })
})

/* El método split() devuelve el nuevo array.

cadena.split([separador][,limite])

Cuando se encuentra, el separador es eliminado de la cadena
 y las subcadenas obtenidas se devuelven en un array.
 Si el separador no es encontrado o se omite,
 el array contendrá un único elemento con la cadena original 
 completa.
 Si el separador es una cadena vacía la cadena es convertida
 en un array de carácteres. */

/*
 arr.join([separator])
 Es una cadena usada para separar cada uno de los elementos
 del arreglo. El separador es convertido a una cadena si es
 necesario. Si este se omite, los elementos del arreglo son
 separados con una coma (","). Si el separador es una cadena
 vacía todos los elementos son unidos sin ningún carácter
 entre ellos.
 */