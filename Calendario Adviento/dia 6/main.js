/*
Give an array of strings, sort them in the order of increasing lengths.
If two strings have the same length, their relative order must be
the same as in the initial array.
*/


/* Código */
function comparar(a, b) {
    if (a.length < b.length)
        return -1;
    if (a.length > b.length)
        return 1;
    return 0;
}

function sortByLength(strs) {
    return strs.sort(comparar);
}

/* Test Jasmine */
describe("sortByLength()", function() {
	it("sorts the strings from shortest to longest", function() {
		const strs = ["abc", "", "aaa", "a", "zz"];
        const result = sortByLength(strs);
        expect(result).toEqual(["", "a", "zz", "abc", "aaa"]);
	})
})

/*
function compare(a, b) {
  if (a es menor que b según criterio de ordenamiento) {
    return -1;
  }
  if (a es mayor que b según criterio de ordenamiento) {
    return 1;
  }
  // a debe ser igual b
  return 0;
}
*/