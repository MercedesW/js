/*
Write a function that splits an array(fist argument) into groups the length size
(second argument) and returns thrm as a two-dimensional array.
*/


/* Código */
function chunkyMonkey(values, size) {
    let finalArray = [];
    let firstArray = values.slice(0, size);
    let secondArray = values.slice(size);

    finalArray.push(firstArray);
    finalArray.push(secondArray); 

    return finalArray;
}

/* Test Jasmine */
describe("chunkyMonkey()", function() {
    it("returns largest positive integer possible for digit count", function() {
        const values = ["a", "b", "c", "d"];
        const size = 2;
        const result = chunkyMonkey(values, size);

        expect(result).toEqual([["a", "b"], ["c", "d"]]);
    })
})

/*
El método slice() devuelve una copia de una parte del array
dentro de un nuevo array empezando por inicio hasta fin (fin no incluido).
El array original no se modificará.
Si fin es omitido, slice extrae hasta el final de la secuencia (arr.length).
*/