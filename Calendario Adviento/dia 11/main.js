/*
AVOID OBSTACLES
DESCRIPTION:
You are given an array of integers representing coordinates of
obstacles situated on a straght line.
Assume that you are jumping from the point with coordinate 0 to the right.
You are allowed only to make jumps of the same length represent by some integer.
Find the minimal length of the jump enough to avoid all the obstacles.

sort()
every()

*/

function avoidObstacles(nums) {
    //  write code here.
}



/**
* Test Suite 
*/
describe('avoidObstacles()', () => {
    it('returns minimal number of jumps in between numbers', () => {
        // arrange
        const nums = [5, 3, 6, 7, 9];
        
        // act
        const result = avoidObstacles(nums);

        // log
        console.log("result: ", result);
        
        // assert
        expect(result).toBe(4);
    });
});