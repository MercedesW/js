/*
You are given a string s that consists of only lowercase English letters.
If vowels('a', 'e', 'i', 'o', 'u') are given a value of 1 and
consonants are given a value of 2,
return the sum of all of the letters in the input string.
*/



/* Código */
function isVowel(vowel) {
    return (vowel === 'a' || vowel === 'e' ||
        vowel === 'i' || vowel === 'o' || vowel ==='u');
}

function countVowelConsonant(str) {
    let array = str.split("");
    const reducer = (sum, value) => {
        /*sum = isVowel(value) ? sum += 1 : sum += 2;*/
        if (isVowel(value)) {
            sum += 1;
        }
        else {
            sum += 2;
        }
        return sum
    };
    const totalCount = array.reduce(reducer, 0);
    return totalCount;
}


/* Test Jasmine */
describe("countVowelConsonant()", function() {
    it("", function() {
        const str = 'abcde';
        const result = countVowelConsonant(str);
        expect(result).toBe(8);
    })
})

/* 
reduce(callback, valor inicial de la suma):

- sino se le da el valor inicial, toma el primer valor del array.

callback(acumulador, valor_actual) => acumulador + valor_actual
*/