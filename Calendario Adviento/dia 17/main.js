/*
Different symbols naive
DESCRIPTION:
Given a string, find the number of diffenrent characters in it.

includes()
split()
push()

*/
function differentSymbolsNaive(str) {
    //  write code here.
}



/**
* Test Suite 
*/
describe('differentSymbolsNaive()', () => {
    it('returns count of unique characters', () => {
        // arrange
        const str = 'cabca';
        
        // act
        const result = differentSymbolsNaive(str);

        // log
        console.log("result: ", result);
        
        // assert
        expect(result).toBe(3);
    });
});